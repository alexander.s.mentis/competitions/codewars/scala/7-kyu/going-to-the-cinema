object Movie {

  def movie(card: Int, ticket: Int, perc: Double): Int = {
    // your code
    
    def times(a: Int, b: Double, count: Int): Int = {
      if (math.ceil(b) < a) count-1
      else times(a+ticket, b + ticket*math.pow(perc, count), count+1)
    }
    
    times(0, card, 1)
  }    
}